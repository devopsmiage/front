import Link from "next/link";

interface NavButtonProps {
    title: string;
    href: string;
}

export default function NavButton({ title, href } : NavButtonProps) {
    return <Link href={href}>{title}</Link>
}
