import styles from './(dashboard-styles)/dashboard-layout.module.css';

export default function DashboardLayout({
    children,
  }: {
    children: React.ReactNode
  }) {
    return (
      <section className={styles.layout}>
        <h1>Dashboard shared layout</h1>
        {children}
      </section>
    )
  }