import styles from './(dashboard-styles)/dashboard-page.module.css';
import NavButton from '@/components/navButton';

export default function Page() {
  return (
    <>
      <h1 className={styles.text}>Hello, Dashboard Page!</h1>
      <NavButton title="Navigate to Settings >" href="/dashboard/settings" />
      <h1></h1>
      <NavButton title="< Navigate to Home" href="/" />
    </>
  )
}

