"use client";
import NavButton from "@/components/navButton";
import styles from "./(settings-styles)/settings-page.module.css";
import axios from "axios";
import { useEffect, useState } from "react";

interface User {
  name: string;
}

export default function Page() {
  const [users, setUsers] = useState<User[]>([]);

  useEffect(() => {
    axios.get('/api/users')
      .then(response => {
        setUsers(response.data);
      })
      .catch(error => {
        console.error('Error fetching users:', error);
      });
  }, []);

    return (
      <>
        <h1 className={styles.text}>Hello, Settings Page!</h1>
        <NavButton title="< Navigate to Dashboard" href="/dashboard" />
        <div>
          <h1>Users</h1>
          <ul>
            {users.map((user, index) => (
              <li key={index}>{user.name}</li>
            ))}
          </ul>
        </div>
      </>
    )
  }