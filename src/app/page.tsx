import NavButton from "@/components/navButton";
import styles from "./(app-styles)/app-page.module.css";

export default function Home() {
  return (
    <> 
      <h1 className={styles.text}>This is the main page</h1>
      <NavButton title="Navigate to Dashboard >" href="/dashboard" />
    </>
  );
}
