import { PHASE_DEVELOPMENT_SERVER, PHASE_PRODUCTION_BUILD } from 'next/constants.js';

export default (phase) => {
  if (phase === PHASE_DEVELOPMENT_SERVER) {
    return {
      async rewrites() {
        return [
          {
            source: '/api/:path*',
            destination: 'http://localhost:4000/:path*' // Proxy to Backend during development
          }
        ];
      }
    };
  }

  if (phase === PHASE_PRODUCTION_BUILD) {
    return {
      async rewrites() {
        return [
          {
            source: '/api/:path*',
            destination: 'https://clhobz-web-app-backend.azurewebsites.net/:path*' // Proxy to Backend in production
          }
        ];
      }
    };
  }

  return {};
};
